
# Run:
# python3 setup_win.py bdist_msi
# to produce installer for Windows

from cx_Freeze import setup, Executable

build_exe_options = {
    'includes': ('queue', 'codecs'),
    'packages': ('colorama',),
    'include_msvcr': True
}

setup(
    long_description='Retrieve list of business owners from a ward',
    name='BusinessOwnerRetriever',
    version='2.0.1',
    author='Nguyễn Hồng Quân',
    author_email='ng.hong.quan@gmail.com',
    options={
        'build_exe': build_exe_options,
        'bdist_msi': {'add_to_path': True, 'all_users': True}
    },
    packages=[],
    package_data={},
    executables=[Executable("businessowner_retriever.py", targetName="businessowner-retriever",
                            copyright="Nguyễn Hồng Quân")],
)
